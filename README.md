# AakashSMS  #

Aakash SMS is a leading Bulk SMS Service provider in Nepal providing organizations and individuals, mobile marketing tools via SMS. It lets you deliver SMS messages to mobile handsets almost anywhere in Nepal regardless of what service provider you use- NTC or Ncell.



### Feature of AakashsSMS ###

* Bulk SMS
* API SMS
* SMS Campaign
* Interactive SMS

### Bulk SMS ###

Bulk Messaging is the dissemination of large number of SMS messages for delivery to mobile phones directly from a web application. Example: alerts, reminders and marketing messages can be sent to customers, staff and other stakeholders.

### API SMS ###

API’s give our customers the capacity to integrate SMS into all facets of their business – applications, websites, intranets, CRM’s, ERP’s and other corporate software – providing a real-time messaging capability in existing corporate applications.

* Url: https://aakashsms.com/admin/public/sms/v1/send
* Parameter: auth_token,from,to,text
* Method: POST / GET

| Field     | Type   | Description                             |
| ----------|:------:| -----:                                  |
| auth_token| string | Token generated from our website.       |
| from      | string | 31001                                   |
| to        | string | Comma Separated 10-digit mobile numbers.|
| text      | string | SMS Message to be sent.                 |



## Also find the examples for API Integration below : ##

### API - Examples using POST method  ###

#### CURL ####

```curl

curl -s http://aakashsms.com/admin/public/sms/v1/send/ \
    -F token='<token from dashboard>' \
    -F from=''31001'' \
    -F to='<comma separated 10 digits mobile numbers>' \
    -F text='<message to be sent>'

```

#### PHP ####

```php
	
	$args = http_build_query(array(
	        'auth_token'=> '<token from dashboard>',
	        'from'  => '31001',
	        'to'    => '<comma separated 10 digits mobile numbers>',
	        'text'  => '<message to be sent>'));
	$url = "http://aakashsms.com/admin/public/sms/v1/send/";

	# Make the call using API.
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_POST, 1); ///
	curl_setopt($ch, CURLOPT_POSTFIELDS,$args);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
	// Response
	$response = curl_exec($ch);
	curl_close($ch);			

```

#### PYTHON ####

```python

	import requests
	r = requests.post(
	        "http://aakashsms.com/admin/public/sms/v1/send/",
	        data={ auth_token: '<token from dashboard>',
	              'from'  : '31001',
	              'to'      : '<comma separated 10 digits mobile numbers>',
	              'text'    : '<message to be sent>'})
	status_code = r.status_code
	response = r.text
	response_json = r.json()

```

#### C# (C-sharp) ####

```Csharp

using System.Collections.Specialized;
using System.Net;
using System.Text;

namespace PostSMS{

    class Program{

        static void Main(string[] args){
                var responseTest = SendSMS('31001', '<token from dashboard>', '<comma separated 10 digits mobile numbers>', '<message to be sent>');
        }

        private static string SendSMS(string from, string token, string to, string text){

            using (var client = new WebClient()){
                var values = new NameValueCollection();
                values["from"] = from;
                values["auth_token"] = token;
                values["to"] = to;
                values["text"] = text;
                var response = client.UploadValues( "http://aakashsms.com/admin/public/sms/v1/send/",  "Post", values);
                var responseString = Encoding.Default.GetString(response);
                return responseString;
            }
        }
    }
}

```

## Response ##

```

{
    'response' => 'A required field is missing',
    'response_code' => 4000
}
{
    'response' => 'Invalid IP Address',
    'response_code' => 4001
}
{
    'response' => 'Invalid URL',
    'response_code' => 4002
}
{
    'response' => 'Invalid AuthToken',
    'response_code' => 4003
}
{
    'response' => 'Account is not active',
    'response_code' => 4004
}
{
    'response' => 'Account has been expired',
    'response_code' => 4005
}
{
    'response' => 'Invalid phone number',
    'response_code' => 4006
}
{
    'response' => 'Invalid sender',
    'response_code' => 4007
}
{
    'response' => 'Text cannot be empty',
    'response_code' => 4008
}
{
    'response' => 'No Credits Available',
    'response_code' => 4009
}
{
    'response' => 'Insufficient Credits',
    'response_code' => 4010
}

```


## Credit check ##

* URL :: https://aakashsms.com/admin/public/sms/v1/credit
* Http Request : POST
* Field Parameter : auth_token   (Your Api Token Value)

| Field     | Type   | Description                             |
| ----------|:------:| -----:                                  |
| auth_token| string | Token generated from our website.       |


#### Responses ####

```
{

    “available_credit”: xxx,
    “total_sms_sent”: xxx,
    “response_code”: 202

}

```


### Report API SMS

* URL: https://aakashsms.com/admin/public/sms/v1/report/api 
* Method: POST
* Parameters: auth_token, page

| Field     | Type    | Description                             |
| ----------|:------: | -----:                                  |
| auth_token| string  | Token generated from our website.       |
| page      | integer | Start from 1,2......


### Pull/Incoming API 

* URL: Public URL where the incoming SMS has to be forwarded.
* Method: GET

GET request is sent to the URL provided with following arguments:


| Field       | Description                             |
| ----------- | -----:                                  |
| keyword     | Get your Keyword                        |
| from        | Mobile number from where message coming |
| text        | Message send by user to your campaign   |


### Pull/Incoming Custom Campaign API 

* URL: Public URL where the incoming SMS has to be forwarded and get response from your end as a reply message to user.
* Method: GET

GET request is sent to the URL provided with following arguments:


| Field       | Description                             |
| ----------- | -----:                                  |
| keyword     | Get your Keyword                        |
| from        | Mobile number from where message coming |
| text        | Message send by user to your campaign   |

#### Response 

You need to build your logic on how to respond to the incoming request and send text response of maximum 160 characters with response code. Incase of any other response code, default reply will be sent to user as SMS reply.


```
{"success":True,"message":"Your Custom Auto reply Message"}

```

| Field       | Description                             |
| ----------- | -----:                                  |
| success       | True/False                              |
| message     | Your Custom Auto reply Message         |



### SEE Result

```
Type see<space>SYMBOL NUMBER  and send it to 31003.

```

### NEB Result

```
Type neb<space>SYMBOL NUMBER  and send it to 31003.

```



### SMS Campaign ###

We can create individual campaigns to generate leads for your products and services. This helps you gather data on your prospective customers, who are most likely to buy your product/service.

* Voting
* Poll
* Quize
* Instant

### SMS Voting ###

SMS Poll is the easiest and fastest way to find out what your audience is thinking. With a simple text message, you can launch a interactive mobile poll that is both engaging for your audience and rewarding for your business.

## Interactive SMS


### 1. Send Interactive SMS using Form-Data

* URL: https://aakashsms.com/admin/public/sms/v1/send/interactive-sms 
* Method: POST
* Parameters: auth_token, campaign_id, text, to

| Field      | Type    | Description                             |
| ---------- |:------: | -----:                                  |
| auth_token | string  | Token generated from our website.       |
| campaign_id| integer | Get from dashboard                      |
| to         | string  | Comma Separated 10-digit mobile numbers.|
| text       | string  | SMS Message to be sent.                 |


## Also find the examples for Interactive API Integration below : ##

### Interactive API - Examples using POST method  ###

#### PHP ####

```php

$post = [
    'auth_token'=> '<token from dashboard>',
    'campaign_id'  => 'get from dashboard',
    'to'    => '<comma separated 10 digits mobile numbers>',
    'text'  => '<message to be sent>'
];

$ch = curl_init('https://aakashsms.com/admin/public/sms/v1/send/interactive-sms');
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_POSTFIELDS, $post);

// execute!
$response = curl_exec($ch);

// close the connection, release resources used
curl_close($ch);

```

### 2. Send Interactive SMS using JSON

* URL: https://aakashsms.com/admin/public/sms/v1/send/interactive-sms-json  
* Method: POST
* Request: 

`````
{

    "auth_token": "YOUR AUTH TOKEN",
    "to": "COMMA SEPARATED MOBILE NUMBER",
    "text": "MESSAGE YOU WANT TO SEND",
    "campaign_id": "YOU GET THIS NUMBER AFTER YOU CREATE CAMPAIGN FROM DASHBOARD"
}

`````

### 3. Report - Interactive SMS

* URL: https://aakashsms.com/admin/public/sms/v1/report/interactive-sms  
* Method: POST
* Parameters: auth_token, campaign_id, page

| Field      | Type    | Description                             |
| ---------- |:------: | -----:                                  |
| auth_token | string  | Token generated from our website.       |
| campaign_id| integer | Get from dashboard                      |
| page       | integer | Start from 1,2...                       |

### Interactive API Report - Examples using POST method  ###

#### PHP ####

```php

$post = [
    'auth_token'=> '<token from dashboard>',
    'campaign_id'  => 'get from dashboard',
    'page'    => '<start from 1>'
];

$ch = curl_init('https://aakashsms.com/admin/public/sms/v1/report/interactive-sms');
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_POSTFIELDS, $post);

// execute!
$response = curl_exec($ch);

// close the connection, release resources used
curl_close($ch);

```

## Contact Information
* Email :info@aakashtech.com.np
* Tel :+977 – 1-4411294 , +977 – 1-4419985
* Address :R.R. Building, Narayanchaur, Naxal, Kathmandu, Nepal
* Url :www.aakashsms.com



